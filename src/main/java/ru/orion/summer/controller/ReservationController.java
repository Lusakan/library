package ru.orion.summer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.orion.summer.dto.security.MessageResponse;
import ru.orion.summer.entity.Person;
import ru.orion.summer.service.interfaces.PersonService;
import ru.orion.summer.service.interfaces.ReservationService;

@RestController
public class ReservationController {
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private PersonService personService;

    final static Logger logger = LoggerFactory.getLogger(ReservationController.class);

    @DeleteMapping("/reservation/cancel/{bookId}")
    public ResponseEntity<?> cancelReservation(@PathVariable Long bookId, Authentication auth){
        Person personFromAuth = personService.getPersonFromAuth(auth);

        if(reservationService.cancelReservation(bookId, personFromAuth.getId())){
            logger.info("User with email: {} canceled reservation for book: {}", personFromAuth.getEmail(),
                    bookId);
            return new ResponseEntity<>(new MessageResponse("Reservation cancelled successfully"), HttpStatus.OK);
        }

        return new ResponseEntity<>(new MessageResponse("There is no such reservation"), HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/reservation/expand/{bookId}")
    public ResponseEntity<?> expandReservation(@PathVariable Long bookId, Authentication auth){
        Person personFromAuth = personService.getPersonFromAuth(auth);

        if (reservationService.expandReservation(bookId, personFromAuth.getId())) {
            logger.info("User with email: {} extended reservation for book with id: {}", personFromAuth.getEmail(),
                    bookId);
            return new ResponseEntity<>(new MessageResponse("Reservation extended for 7 days"), HttpStatus.OK);
        }

        return new ResponseEntity<>(new MessageResponse("There is no such reservation"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/reservation/create/{bookId}")
    public ResponseEntity<?> createReservation(@PathVariable Long bookId, Authentication auth){
        Person person = personService.getPersonFromAuth(auth);
        if(reservationService.createReservation(bookId, person.getId())){
            logger.info("User with email: {} reserved book with id: {}", person.getEmail(), bookId);
            return new ResponseEntity<>(new MessageResponse("Reservation successfully created book: "
                    + bookId + ", person: " + person.getId()), HttpStatus.OK);
        }

        return new ResponseEntity<>(new MessageResponse("Can't create reservation for book: " + bookId
        + ", person: " + person.getId()), HttpStatus.BAD_REQUEST);
    }

}
