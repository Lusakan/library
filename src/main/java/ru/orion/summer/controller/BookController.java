package ru.orion.summer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.orion.summer.dto.BookDTO;
import ru.orion.summer.dto.security.MessageResponse;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.enums.BookStatus;
import ru.orion.summer.service.interfaces.BookService;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @PostMapping("/book/add")
    public ResponseEntity<?> addBook(@RequestBody BookDTO bookInfo){
        if (bookService.exists(bookInfo.getName())) {
            return new ResponseEntity<>(new MessageResponse("Book with name: " + bookInfo.getName() + " already exists"),
                    HttpStatus.BAD_REQUEST);
        }
        bookService.addBook(bookInfo);
        return new ResponseEntity<>(new MessageResponse("The book was successfully added"), HttpStatus.OK);
    }

    @GetMapping("/book/all")
    public ResponseEntity<?> getAllBooks(){
        return new ResponseEntity<>(bookService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/book/status/{bookStatus}")
    public ResponseEntity<?> getAllByStatus(@PathVariable BookStatus bookStatus){
        return new ResponseEntity<>(bookService.getAllByStatus(bookStatus), HttpStatus.OK);
    }

    @GetMapping("/book/{bookId}")
    public ResponseEntity<Book> getBookById(@PathVariable Long bookId){
        Book bookById = bookService.findBookById(bookId);
        return new ResponseEntity<>(bookById, HttpStatus.OK);
    }
}
