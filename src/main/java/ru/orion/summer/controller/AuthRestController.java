package ru.orion.summer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.orion.summer.dto.security.SignInDTO;
import ru.orion.summer.dto.security.SignUpDTO;
import ru.orion.summer.service.interfaces.PersonService;

@RestController
@RequestMapping("/auth")
public class AuthRestController {
    @Autowired
    private PersonService personService;

    @PostMapping("/sign_up")
    public ResponseEntity<?> signUp(@RequestBody SignUpDTO userInfo){
        return personService.signUp(userInfo);
    }

    @PostMapping("/sign_in")
    public ResponseEntity<?> signIn(@RequestBody SignInDTO userInfo){
        return personService.signIn(userInfo);
    }
}
