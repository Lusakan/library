package ru.orion.summer.controller.viewController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthViewController {

    @GetMapping("/sign_up")
    public String getSignUpPage(){
        return "signUp";
    }

    @GetMapping("/sign_in")
    public String getSignInPage(){
        return "signIn";
    }
}
