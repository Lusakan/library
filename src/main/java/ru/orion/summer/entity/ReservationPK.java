package ru.orion.summer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@Embeddable
public class ReservationPK implements Serializable {

    public ReservationPK(Long bookId, Long personId){
        this.bookId = bookId;
        this.personId = personId;
    }

    private Long personId;

    private Long bookId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationPK that = (ReservationPK) o;
        return Objects.equals(personId, that.personId) && Objects.equals(bookId, that.bookId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personId, bookId);
    }
}
