package ru.orion.summer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ru.orion.summer.entity.enums.BookStatus;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "books")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "reservation")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "book_name")
    private String bookName;

    @Column(name = "author_name")
    private String authorName;

    @Column(name = "author_surname")
    private String authorSurname;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private BookStatus status;

    @OneToOne(mappedBy = "book", cascade = CascadeType.ALL)
    Reservation reservation;
}
