package ru.orion.summer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Entity
@Table(name = "reservations")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reservation implements Serializable {

    @EmbeddedId
    private ReservationPK reservationPK = new ReservationPK();

    @ManyToOne
    @JsonIgnore
    @MapsId("personId")
    @JoinColumn(name = "person_id")
    private Person person;

    @OneToOne
    @JsonIgnore
    @MapsId("bookId")
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "end_date")
    private LocalDate endDate;

}
