package ru.orion.summer.entity.enums;

public enum BookStatus {
    FREE, RESERVED
}
