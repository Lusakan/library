package ru.orion.summer.entity;

public enum BookStatus {FREE_FOR_RESERVATION, RESERVED}
