package ru.orion.summer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.enums.BookStatus;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    boolean existsByBookNameIgnoreCase(String bookName);
    List<Book> findAllByStatus(BookStatus status);
}
