package ru.orion.summer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.Person;
import ru.orion.summer.entity.Reservation;
import ru.orion.summer.entity.ReservationPK;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, ReservationPK>{
    List<Reservation> findAllByPerson(Person person);

    Optional<Reservation> findFirstByBookAndPerson(Book book, Person person);

    Optional<Reservation> findFirstByBookIdAndPersonId(Long bookID, Long personID);

    boolean existsByBookIdAndPersonId(Long bookId, Long personId);

    boolean existsByBookId(Long bookId);

    List<Reservation> findAllByEndDateBefore(LocalDate date);

    @Modifying
    @Query("delete from Reservation t where t.reservationPK = ?1")
    void delete(ReservationPK pk);
}
