package ru.orion.summer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.orion.summer.dto.BookDTO;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.enums.BookStatus;
import ru.orion.summer.repository.BookRepository;
import ru.orion.summer.service.interfaces.BookService;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book findBookById(Long bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isPresent()) return book.get();
        throw new NoSuchElementException("There is no book with id: " + bookId + " in database");
    }

    @Override
    public boolean exists(String bookName) {
        return bookRepository.existsByBookNameIgnoreCase(bookName);
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void addBook(BookDTO bookInfo) {
        Book book = bookInfo.getBook();
        save(book);
    }

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> getAllByStatus(BookStatus bookStatus) {
        return bookRepository.findAllByStatus(bookStatus);
    }
}
