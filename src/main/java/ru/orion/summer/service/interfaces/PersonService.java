package ru.orion.summer.service.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import ru.orion.summer.dto.security.SignInDTO;
import ru.orion.summer.dto.security.SignUpDTO;
import ru.orion.summer.entity.Person;

public interface PersonService {

    Person findByEmail(String personEmail);

    Person findById(Long personId);

    void save(Person person);

    Person getPersonFromAuth(Authentication auth);

    ResponseEntity<?> signUp(SignUpDTO userInfo);

    ResponseEntity<?> signIn(SignInDTO userInfo);
}
