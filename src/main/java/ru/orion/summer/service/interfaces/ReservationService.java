package ru.orion.summer.service.interfaces;

import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.Person;
import ru.orion.summer.entity.Reservation;

import java.util.List;


public interface ReservationService {

    boolean cancelReservation(Long bookId, Long personId);

    boolean expandReservation(Long bookId, Long personId);

    boolean createReservation(Long bookId, Long personId);

    List<Reservation> getOutdated();
}
