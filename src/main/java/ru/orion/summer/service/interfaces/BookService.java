package ru.orion.summer.service.interfaces;

import ru.orion.summer.dto.BookDTO;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.enums.BookStatus;

import java.util.List;

public interface BookService {

    Book findBookById(Long bookId);
    boolean exists(String bookName);
    void save(Book book);

    void addBook(BookDTO bookInfo);

    List<Book> findAll();

    List<Book> getAllByStatus(BookStatus bookStatus);
}
