package ru.orion.summer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.orion.summer.dto.security.JwtResponse;
import ru.orion.summer.dto.security.MessageResponse;
import ru.orion.summer.dto.security.SignInDTO;
import ru.orion.summer.dto.security.SignUpDTO;
import ru.orion.summer.entity.Person;
import ru.orion.summer.repository.PersonRepository;
import ru.orion.summer.securityThings.jwt.JwtUtils;
import ru.orion.summer.securityThings.services.UserDetailsImpl;
import ru.orion.summer.service.interfaces.PersonService;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    PersonRepository personRepository;


    public Person findByEmail(String email){
        Optional<Person> person = personRepository.findByEmail(email);
        if (person.isPresent()) return person.get();
        throw new UsernameNotFoundException("There is no person with email: " + email +" in database");
    }

    public Person findById(Long id){
        Optional<Person> byId = personRepository.findById(id);
        if(byId.isPresent()) return byId.get();
        throw new NoSuchElementException("There is no person with id: " + id);
    }

    public void save(Person person){
        personRepository.save(person);
    }

    public Person getPersonFromAuth(Authentication auth){
        return ((UserDetailsImpl) auth.getPrincipal()).getPerson();
    }

    public ResponseEntity<?> signUp(SignUpDTO userInfo) {
        if (personRepository.existsByEmailIgnoreCase(userInfo.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already taken!"));
        }
        Person user = userInfo.getPersonUser(passwordEncoder);

        personRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    public ResponseEntity<?> signIn(SignInDTO userInfo) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userInfo.getEmail(), userInfo.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String role = userDetails.getPerson().getRole().name();

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                role));
    }
}
