package ru.orion.summer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.Person;
import ru.orion.summer.entity.Reservation;
import ru.orion.summer.entity.ReservationPK;
import ru.orion.summer.entity.enums.BookStatus;
import ru.orion.summer.repository.ReservationRepository;
import ru.orion.summer.service.interfaces.BookService;
import ru.orion.summer.service.interfaces.PersonService;
import ru.orion.summer.service.interfaces.ReservationService;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {
    @Autowired
    private PersonService personService;
    @Autowired
    private BookService bookService;
    @Autowired
    private ReservationRepository reservationRepository;


    @Override
    public boolean cancelReservation(Long bookId, Long personId) {
        try {

            Reservation reservation = findByBookIdAndPersonId(bookId, personId);
            reservationRepository.delete(reservation.getReservationPK());

            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    public Reservation findByBookIdAndPersonId(Long bookId, Long personId) {
        Optional<Reservation> reservation = reservationRepository.findFirstByBookIdAndPersonId(bookId, personId);
        if (reservation.isPresent()) return reservation.get();
        throw new NoSuchElementException("There is no reservation with book: "
                + bookId + " and person: " + personId);
    }

    @Override
    public boolean expandReservation(Long bookId, Long personId) {
        Optional<Reservation> reservation = reservationRepository.findFirstByBookIdAndPersonId(bookId, personId);
        if (reservation.isPresent()) {
            Reservation res = reservation.get();
            res.setEndDate(res.getEndDate().plusDays(7));
            reservationRepository.save(res);
            return true;
        }
        return false;
    }

    @Override
    public boolean createReservation(Long bookId, Long personId) {
        if(reservationRepository.existsByBookId(bookId)) return false;

        try {
            Person person = personService.findById(personId);
            Book book = bookService.findBookById(bookId);

            Reservation reservation = Reservation.builder()
                    .person(person)
                    .book(book)
                    .endDate(LocalDate.now().plusDays(7))
                    .build();

            book.setStatus(BookStatus.RESERVED);
            bookService.save(book);

            person.getReservations().add(reservation);
            personService.save(person);

            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Override
    public List<Reservation> getOutdated() {
        return reservationRepository.findAllByEndDateBefore(LocalDate.now());
    }


}
