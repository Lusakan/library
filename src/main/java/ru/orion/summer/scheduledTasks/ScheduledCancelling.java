package ru.orion.summer.scheduledTasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.orion.summer.entity.Reservation;
import ru.orion.summer.service.interfaces.ReservationService;

import java.util.List;

@Component
public class ScheduledCancelling {

    @Autowired
    private ReservationService reservationService;

    @Scheduled(cron = " 0 0 0 * * *")
    public void cancelOutdatedReservations(){
        List<Reservation> outdated = reservationService.getOutdated();
        for (Reservation res:outdated) {
            reservationService.cancelReservation(res.getReservationPK().getBookId(),
                    res.getReservationPK().getBookId());
        }
    }
}
