package ru.orion.summer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.orion.summer.entity.Book;
import ru.orion.summer.entity.enums.BookStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookDTO {
    private String name;
    private String authorName;
    private String authorSurname;

    public Book getBook(){
        return Book.builder()
                .authorName(authorName)
                .authorSurname(authorSurname)
                .bookName(name)
                .status(BookStatus.FREE)
                .build();
    }
    public static BookDTO toBookDTO(Book book){
        return new BookDTO(book.getBookName(), book.getAuthorName(), book.getAuthorSurname());
    }
}
