package ru.orion.summer.dto.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.orion.summer.entity.Person;
import ru.orion.summer.entity.enums.ERole;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpDTO {
    private String name;
    private String surname;
    private String email;
    private String password;

    public Person getPersonUser(PasswordEncoder passwordEncoder){
        Person p = Person.builder()
                .name(this.name)
                .surname(this.surname)
                .email(this.email)
                .password(passwordEncoder.encode(this.password))
                .role(ERole.USER)
                .build();
        return p;
    }
}
