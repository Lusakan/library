
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign-Up!</title>
</head>
<body>
<h1>Please sign-up!</h1>

<form action="/auth/sign_up" method="post">

    Name:
    <input type="text" name="name">
    <br>
    Surname:
    <input type="text" name="surname">
    <br>
    Email:
    <input type="text" name="email">
    <br>
    Password:
    <input type="password" name="password">
    <br>
    <br>

    <button type="submit" value="Submit!"/>
</form>
</body>
</html>
